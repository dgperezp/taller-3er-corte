const { Client, LocalAuth } = require("whatsapp-web.js");
const qrcode = require("qrcode-terminal");
const moment = require("moment-timezone");
const colors = require("colors");
const fs = require("fs");

const client = new Client({
  restartOnAuthFail: true,
  puppeteer: {
    headless: true,
    args: ["--no-sandbox", "--disable-setuid-sandbox"],
  },
  ffmpeg: "./ffmpeg.exe",
  authStrategy: new LocalAuth({ clientId: "client" }),
});
const config = require("./config/config.json");

client.on("qr", (qr) => {
  console.log(
    `[${moment().tz(config.timezone).format("HH:mm:ss")}] Scan the QR below : `
  );
  qrcode.generate(qr, { small: true });
});

client.on("ready", () => {
  console.clear();
  const consoleText = "./config/console.txt";
  fs.readFile(consoleText, "utf-8", (err, data) => {
    if (err) {
      console.log(
        `[${moment()
          .tz(config.timezone)
          .format("HH:mm:ss")}] Console Text not found!`.yellow
      );
      console.log(
        `[${moment().tz(config.timezone).format("HH:mm:ss")}] ${
          config.name
        } is Already!`.green
      );
    } else {
      console.log(data.green);
      console.log(
        `[${moment().tz(config.timezone).format("HH:mm:ss")}] ${
          config.name
        } is Already!`.green
      );
    }
  });
});

client.on("message", async (message) => {
  const isGroups = message.from.endsWith("@g.us") ? true : false;
  if ((isGroups && config.groups) || !isGroups) {

    if (message.type == "chat") {
      const mensajeUsuario = message.body.toLowerCase();

      if (mensajeUsuario === "1") {
        const currentHour = moment().tz(config.timezone).format("HH");
        let greetingMessage;

        if (currentHour >= 5 && currentHour < 12) {
          greetingMessage = "Hola, buenos días.";
        } else if (currentHour >= 12 && currentHour < 18) {
          greetingMessage = "Hola, buenas tardes.";
        } else {
          greetingMessage = "Hola, buenas noches.";
        }

        // Envía el saludo
        client.sendMessage(message.from, greetingMessage);
      // ...
    }else if (mensajeUsuario === "2") {
        const currentDateTime = moment().tz(config.timezone).format("YYYY-MM-DD HH:mm:ss");
        const responseMessage = `La fecha y hora actual es: ${currentDateTime}`;
        client.sendMessage(message.from, responseMessage);
  }
  // ...
  else {
        client.getChatById(message.id.remote).then(async (chat) => {
          await chat.sendSeen();
        });
      }
    }
  }
});

client.initialize();
